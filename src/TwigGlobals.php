<?php

/**
 * @file
 * Contains \Drupal\twig_globals\TwigGlobals.
 */

namespace Drupal\twig_globals;

use Drupal\Core\Path\PathMatcherInterface;

/**
 * Provides basic Drupal system variables.
 */
class TwigGlobals {

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The active theme.
   *
   * @var \Drupal\Core\Theme\ActiveTheme
   */
  protected $activeTheme;

  /**
   * The currently logged in user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;


  public function getBasePath() {
    global $base_path;
    return $base_path;
  }

  public function getActiveThemeName() {
    if (!isset($this->activeTheme)) {
      $this->activeTheme = \Drupal::service('theme.manager')->getActiveTheme();
    }
    return $this->activeTheme->getName();
  }

  public function getActiveThemePath() {
    if (!isset($this->activeTheme)) {
      $this->activeTheme = \Drupal::service('theme.manager')->getActiveTheme();
    }
    return $this->activeTheme->getPath();
  }

  public function isFrontPage() {
    if (!isset($this->pathMatcher)) {
      $this->pathMatcher = \Drupal::service('path.matcher');
    }
    return $this->pathMatcher->isFrontPage();
  }

  public function isLoggedIn() {
    if (!isset($this->currentUser)) {
      $this->currentUser = \Drupal::currentUser();
    }
    return $this->currentUser->isAuthenticated();
  }

  public function isUserId1() {
    if (!isset($this->currentUser)) {
      $this->currentUser = \Drupal::currentUser();
    }
    return $this->currentUser->id() == 1;
  }

  public function getCurrentUserRoles() {
    if (!isset($this->currentUser)) {
      $this->currentUser = \Drupal::currentUser();
    }
    return $this->currentUser->getRoles();
  }

  public function hasRole($role) {
    if (!isset($this->currentUser)) {
      $this->currentUser = \Drupal::currentUser();
    }
    if (!is_array($role)) {
      $role = [$role];
    }
    $currentRoles = array_flip($this->currentUser->getRoles());
    foreach ($role as $option) {
      if (isset($currentRoles[$option])) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
